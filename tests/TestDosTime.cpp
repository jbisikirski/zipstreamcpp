#include <zip_stream/file.h>
#include <cassert>
#include <iostream>

int main() {
  auto result = zip::dosTime(1416246368);
  std::cerr << result << std::endl;
  assert(result == 1165069764);

  // 1980
  result = zip::dosTime(315532800);
  std::cerr << result << std::endl;
  assert(result == 2162688);

  // 1970
  result = zip::dosTime(0);
  std::cerr << result << std::endl;
  assert(result == 2162688);

  // over 2107
  result = zip::dosTime(4361975642ll);
  std::cerr << result << std::endl;
  assert(result == 4288659325);
  return 0;
}
