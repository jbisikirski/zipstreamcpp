#include <zip_stream.h>
#include <cassert>

int main() {
  zip::ZipStream zip_stream;

  // File content
  std::vector<uint8_t> data{'1', '2', '3', '4'};
  zip::file::Options options;
  options.method = zip::Method::kDEFLATE;
  zip_stream.addData(data, "folder/file.txt", options);

  assert(zip_stream.bufferSize() != 0);
  std::vector<uint8_t> out_buffer;
  out_buffer.reserve(1024);
  zip_stream.fillData(out_buffer.data(), 1024);
  // do something with out_buffer

  // add more files

  zip_stream.finalize();

  assert(zip_stream.isDone());
  assert(zip_stream.bufferSize() != 0);
  // fetch last bits of data
  zip_stream.fillData(out_buffer.data(), 1024);
}
