# ZipStreamCPP

[![pipeline status](https://gitlab.com/jbisikirski/zipstreamcpp/badges/master/pipeline.svg)](https://gitlab.com/jbisikirski/zipstreamcpp/commits/master) [![coverage report](https://gitlab.com/jbisikirski/zipstreamcpp/badges/master/coverage.svg)](https://gitlab.com/jbisikirski/zipstreamcpp/commits/master)

ZipStreamCPP is a c++ library inspired by (blatantly copied from) [ZipStream-PHP](https://github.com/maennchen/ZipStream-PHP)
Library was created to be able to stream zipped that while new data was still being added to the archive. There were no sollutions that I could find, excluding sollutions that involved using huge libraries (Boost, POCO, etc)
This library is meant only for compression, does not provide ability to read zip files;

# Requirements
  - c+\+11 (why aren't you using at least c++11 already?)
  - zlib (version tested 1.2)
  - CMaker (3.2)

# Building
```sh
cmake [-DENABLE_TESTS=OFF] [-DBUILD_SHARED_LIBS=ON]
make
```
Optionally
```sh
make install
```
# Usage
```cpp
#include <zip_stream.h>
#include <cassert>

int main() {
  zip::ZipStream zip_stream;

  // File content
  std::vector<uint8_t> data{'1', '2', '3', '4'};
  zip::file::Options options;
  options.method = zip::Method::kDEFLATE;
  zip_stream.addData(data, "folder/file.txt", options);

  assert(zip_stream.bufferSize() != 0);
  std::vector<uint8_t> out_buffer;
  out_buffer.reserve(1024);
  zip_stream.fillData(out_buffer.data(), 1024);
  // do something with out_buffer

  // add more files

  zip_stream.finalize();

  assert(zip_stream.isDone());
  assert(zip_stream.bufferSize() != 0);
  // fetch last bits of data
  zip_stream.fillData(out_buffer.data(), 1024);
}
```
