cmake_minimum_required(VERSION 3.2 FATAL_ERROR)
set(CMAKE_CXX_STANDARD 11)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_EXTENSIONS OFF)

find_program(CMAKE_CXX_CPPCHECK NAMES cppcheck)
if (CMAKE_CXX_CPPCHECK)
    list(
        APPEND CMAKE_CXX_CPPCHECK
            "--enable=warning,performance,portability"
            "--quiet" #only print when error
    )
endif()

project(zipstreamcpp VERSION 0.1 LANGUAGES CXX)
set(PRETTY_NAME "ZipStreamCPP")

enable_testing()

option(ENABLE_TESTS "Build with unit tests" ON)
option(BUILD_SHARED_LIBS "Build shared lib" OFF)


file(GLOB_RECURSE SOURCE_FILES "src/*.cpp")
add_library(${PROJECT_NAME} ${SOURCE_FILES})

find_package(ZLIB 1.2)

target_link_libraries(${PROJECT_NAME} PUBLIC ZLIB::ZLIB)
target_compile_definitions(${PROJECT_NAME} PUBLIC "ZLIB_CONST")
target_include_directories(${PROJECT_NAME} PUBLIC
    $<BUILD_INTERFACE:${${PROJECT_NAME}_SOURCE_DIR}/include/public>
    $<INSTALL_INTERFACE:include>
    )

target_include_directories(${PROJECT_NAME} PRIVATE
    $<BUILD_INTERFACE:${${PROJECT_NAME}_SOURCE_DIR}/include/private>
    )


set_target_properties(${PROJECT_NAME} PROPERTIES
    CXX_STANDARD 11
    CXX_STANDARD_REQUIRED YES
    CXX_EXTENSIONS NO
)

install(TARGETS ${PROJECT_NAME} EXPORT ${PRETTY_NAME}Targets
  LIBRARY DESTINATION lib
  ARCHIVE DESTINATION lib
  RUNTIME DESTINATION bin
  INCLUDES DESTINATION include/zipstreamcpp
)

install(DIRECTORY "${PROJECT_SOURCE_DIR}/include/private"
    DESTINATION include/zipstreamcpp
    )

install(TARGETS ${PROJECT_NAME}
    DESTINATION lib
    )

include(CMakePackageConfigHelpers)
write_basic_package_version_file(
    "${CMAKE_CURRENT_BINARY_DIR}/${PROJECT_NAME}/${PROJECT_NAME}ConfigVersion.cmake"
    VERSION ${PROJECT_VERSION}
    COMPATIBILITY AnyNewerVersion
)


export(EXPORT ${PRETTY_NAME}Targets
    FILE "${CMAKE_CURRENT_BINARY_DIR}/${PRETTY_NAME}/${PRETTY_NAME}Targets.cmake"
)

configure_file(cmake/${PRETTY_NAME}Config.cmake
    "${CMAKE_CURRENT_BINARY_DIR}/${PRETTY_NAME}/${PRETTY_NAME}Config.cmake"
    COPYONLY
)

set(ConfigPackageLocation lib/cmake/${PROJECT_NAME})
install(EXPORT ${PRETTY_NAME}Targets
  FILE
  ${PRETTY_NAME}Targets.cmake
  DESTINATION
    ${ConfigPackageLocation}
)
install(
  FILES
  cmake/${PRETTY_NAME}Config.cmake
  "${CMAKE_CURRENT_BINARY_DIR}/${PRETTY_NAME}/${PRETTY_NAME}ConfigVersion.cmake"
  DESTINATION
    ${ConfigPackageLocation}
)

if (ENABLE_TESTS)
  add_subdirectory(tests)
endif()
