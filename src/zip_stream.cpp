#include <zip_stream.h>
#include <zip_stream/file.h>
#include <zip_stream/headers.h>
#include <cstring>

namespace zip {
ZipStream::ZipStream(const std::string &comment)
    : comment_(comment),
      file_count_(0),
      total_size_(0),
      buffer_(),
      cdr_(),
      done_(false) {}

ZipStream::~ZipStream() {}

void ZipStream::addData(const std::vector<uint8_t> &data,
                        const std::string &path, file::Options options) {
  if (done_) {
    return;
  }

  size_t offset = total_size_;
  zip::File file(path, *this, options);
  file.processData(data);
  auto file_cdr = file.getCDREntry(offset);
  cdr_.insert(cdr_.end(), file_cdr.begin(), file_cdr.end());
  file_count_++;
}

void ZipStream::append(const std::vector<uint8_t> &data) {
  std::lock_guard<std::mutex> lock(mutex_);
  buffer_.insert(buffer_.end(), data.begin(), data.end());
  total_size_ += data.size();
}

void ZipStream::finalize() {
  if (done_) {
    return;
  }

  size_t cdr_offset = cdr_.size();
  size_t offset = total_size_;
  append(cdr_);
  if (file_count_ >= std::numeric_limits<uint16_t>::max() ||
      cdr_offset >= std::numeric_limits<uint32_t>::max() ||
      offset >= std::numeric_limits<uint32_t>::max()) {
    //    ['V', static::ZIP64_CDR_EOF_SIGNATURE],     // ZIP64 end of central
    //                                                  file header signature
    //    ['P', 44],                                  // Length of data below
    //                                 this header (length of block - 12) = 44
    //    ['v', static::ZIP_VERSION_MADE_BY],         // Made by version
    //    ['v', Version::ZIP64],                      // Extract by version
    //    ['V', 0x00],                                // disk number
    //    ['V', 0x00],                                // no of disks
    //    ['P', $num_files],                          // no of entries on disk
    //    ['P', $num_files],                          // no of entries in cdr
    //    ['P', $cdr_length],                         // CDR size
    //    ['P', $cdr_offset],                         // CDR offset
    std::vector<uint8_t> cdr64{ZIP64_CDR_EOF_SIGNATURE};
    LE64(cdr64, 44);
    LE16(cdr64, zip::Version::kZIP64);
    LE32(cdr64, 0);
    LE32(cdr64, 0);
    LE64(cdr64, file_count_);
    LE64(cdr64, file_count_);
    LE64(cdr64, cdr_offset);
    LE64(cdr64, offset);
    append(cdr64);

    //    ['V', static::ZIP64_CDR_LOCATOR_SIGNATURE], // ZIP64 end of central
    //    ['V', 0x00],                                // Disc number containing
    //    ['P', $cdr_offset],                         // CDR offset
    //    ['V', 1],                                   // Total number of disks
    cdr64 = ZIP64_CDR_LOCATOR_SIGNATURE;
    LE32(cdr64, 0);
    LE64(cdr64, offset + cdr_offset);
    LE32(cdr64, 1);
    append(cdr64);
  }

  //['V', static::CDR_EOF_SIGNATURE],   // end of central file header signature
  //['v', 0x00],                        // disk number
  //['v', 0x00],                        // no of disks
  //['v', min($num_files, 0xFFFF)],     // no of entries on disk
  //['v', min($num_files, 0xFFFF)],     // no of entries in cdr
  //['V', $cdr_length->getLowFF()],     // CDR size
  //['V', $cdr_offset->getLowFF()],     // CDR offset
  //['v', strlen($comment)],            // Zip Comment size
  std::vector<uint8_t> cdr_eof{CDR_EOF_SIGNATURE};
  LE16(cdr_eof, 0);
  LE16(cdr_eof, 0);
  LE16(cdr_eof, std::min<uint64_t>(file_count_, 0xFFFF));
  LE16(cdr_eof, std::min<uint64_t>(file_count_, 0xFFFF));
  LE32(cdr_eof, std::min<uint64_t>(cdr_offset, 0xFFFFFFFF));
  LE32(cdr_eof, std::min<uint64_t>(offset, 0xFFFFFFFF));
  LE16(cdr_eof, comment_.size());

  cdr_eof.insert(cdr_eof.end(), comment_.begin(), comment_.end());

  append(cdr_eof);

  done_ = true;
}

bool ZipStream::isDone() const { return done_; }

size_t ZipStream::getCurrentOffset() const { return total_size_; }

size_t ZipStream::fillData(uint8_t *const buffer, size_t max) {
  if (buffer_.size() == 0 || max == 0) {
    return 0;
  }
  std::lock_guard<std::mutex> lock(mutex_);

  size_t size = std::min<size_t>(max - 1, buffer_.size());
  memcpy(buffer, buffer_.data(), size);
  buffer_.erase(buffer_.begin(), buffer_.begin() + static_cast<ssize_t>(size));

  return size;
}

size_t ZipStream::bufferSize() const { return buffer_.size(); }

}  // namespace zip
