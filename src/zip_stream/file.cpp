#include <endian.h>
#include <zip_stream.h>
#include <zip_stream/crc.h>
#include <zip_stream/file.h>
#include <zip_stream/headers.h>
#include <zlib.h>
#include <cassert>
#include <chrono>
#include <climits>

constexpr size_t chunk_size = 16384;

namespace zip {
uint32_t dosTime(int64_t timestamp) {
  auto epoch = std::chrono::system_clock::time_point();
  auto since_epoch = std::chrono::seconds(timestamp);
  auto time_point = epoch + since_epoch;
  time_t ttime = std::chrono::system_clock::to_time_t(time_point);
  std::tm tm;
  localtime_r(&ttime, &tm);
  // 1980 - 2107
  if (tm.tm_year < 80) {
    tm.tm_year = 80;
    tm.tm_mon = 0;
    tm.tm_mday = 1;
    tm.tm_hour = 0;
    tm.tm_min = 0;
    tm.tm_sec = 0;
  } else if (tm.tm_year > 207) {
    tm.tm_year = 207;
    tm.tm_mon = 11;
    tm.tm_mday = 31;
    tm.tm_hour = 23;
    tm.tm_min = 59;
    tm.tm_sec = 59;
  }

  // remove extra years from 1980
  tm.tm_year -= 80;
  tm.tm_mon += 1;
  return ((tm.tm_year << 25) | (tm.tm_mon << 21) | (tm.tm_mday << 16) |
          (tm.tm_hour << 11) | (tm.tm_min << 5) | (tm.tm_sec >> 1));
}

bool deflate(const std::vector<uint8_t>& in, std::vector<uint8_t>* out,
             int level) {
  int32_t ret, flush;
  size_t have;
  z_stream strm;

  /* allocate deflate state */
  strm.zalloc = Z_NULL;
  strm.zfree = Z_NULL;
  strm.opaque = Z_NULL;
  ret = deflateInit2(&strm, level, Z_DEFLATED, -15, 8, Z_DEFAULT_STRATEGY);
  if (ret != Z_OK) {
    return false;
  }

  size_t left = in.size();
  /* compress until end of file */
  do {
    strm.avail_in =
        static_cast<uint32_t>(left > chunk_size ? chunk_size : left);
    strm.next_in = in.data() + (in.size() - left);
    left -= strm.avail_in;
    flush = left == 0 ? Z_FINISH : Z_NO_FLUSH;

    /* run deflate() on input until output buffer not full, finish
       compression if all of source has been read in */
    do {
      strm.avail_out = chunk_size;
      uint8_t buff[chunk_size];
      strm.next_out = buff;
      ret = deflate(&strm, flush);   /* no bad return value */
      assert(ret != Z_STREAM_ERROR); /* state not clobbered */
      have = chunk_size - strm.avail_out;
      out->insert(out->end(), buff, buff + have);
    } while (strm.avail_out == 0);
    assert(strm.avail_in == 0); /* all input will be used */

    /* done when last data in file processed */
  } while (flush != Z_FINISH);
  assert(ret == Z_STREAM_END); /* stream will be complete */

  /* clean up and return */
  (void)deflateEnd(&strm);
  return true;
}

File::File(const std::string& name, ZipStream& zip_stream,
           const file::Options& options)
    : zip_stream_(zip_stream),
      name_(name),
      size_(0),
      compressed_size_(0),
      header_size_(0),
      footer_size_(0),
      offset_(0),
      total_size_(0),
      dos_time_(dosTime(options.time)),
      config_bits_(0),
      crc_(0),
      options_(options) {}

File::~File() {}

void File::processData(const std::vector<uint8_t>& data) {
  size_ = data.size();
  crc_ = crc32buf(data);
  std::vector<uint8_t> compressed;
  if (options_.method == Method::kDEFLATE) {
    options_.version = Version::kDEFLATE;
    if (!deflate(data, &compressed, Z_DEFAULT_COMPRESSION)) {
      compressed.clear();
      options_.method = Method::kSTORE;
      options_.version = Version::kSTORE;
    }
  } else {
    compressed = data;
  }
  compressed_size_ = compressed.size();
  addHeader();
  zip_stream_.append(compressed);
  addFooter();
}

void File::addHeader() {
  // if deflate set version to deflate

  //  ['V', ZipStream::FILE_HEADER_SIGNATURE],
  //  ['v', $this->version->getValue()],      // Version needed to Extract
  //  ['v', $this->bits],                     // General purpose bit flags -
  //  ['v', $this->method->getValue()],       // Compression method
  //  ['V', $time],                           // Timestamp (DOS Format)
  //  ['V', $this->crc],                      // CRC32 of data (0 -> moved to
  //  ['V', $this->zlen->getLowFF($force)],   // Length of compressed data
  //  ['V', $this->len->getLowFF($force)],    // Length of original data (forced
  //  ['v', $nameLength],                     // Length of filename
  //  ['v', strlen($footer)],                 // Extra data (see above)

  std::vector<uint8_t> extra_data;
  if (size_ >= std::numeric_limits<uint32_t>::max()) {
    LE64(extra_data, size_);
  }
  if (compressed_size_ >= std::numeric_limits<uint32_t>::max()) {
    LE64(extra_data, compressed_size_);
  }
  if (extra_data.size() > 0) {
    std::vector<uint8_t> ext;
    LE16(ext, 0x0001);
    LE16(ext, extra_data.size());
    extra_data.insert(extra_data.begin(), ext.begin(), ext.end());
    options_.version = Version::kZIP64;
  }

  if (zip_stream_.getCurrentOffset() >= std::numeric_limits<uint32_t>::max()) {
    options_.version = Version::kZIP64;
  }

  std::vector<uint8_t> header{FILE_HEADER_SIGNATURE};

  LE16(header, options_.version);
  LE16(header, config_bits_);
  LE16(header, options_.method);
  LE32(header, dos_time_);
  LE32(header, crc_);
  LE32(header, std::min<uint64_t>(compressed_size_, 0xFFFFFFFF));
  LE32(header, std::min<uint64_t>(size_, 0xFFFFFFFF));
  LE16(header, name_.size());
  LE16(header, extra_data.size());

  header.insert(header.end(), name_.begin(), name_.end());
  header.insert(header.end(), extra_data.begin(), extra_data.end());

  zip_stream_.append(header);

  header_size_ = header.size();
}

void File::addFooter() {
  footer_size_ = 0;
  // nothing yet
  // @TODO add ZERO HEADER handling
  total_size_ = header_size_ + compressed_size_ + footer_size_;
}

std::vector<uint8_t> File::getCDREntry(size_t in_archive_offset) {
  //  ['V', ZipStream::CDR_FILE_SIGNATURE],   // Central file header signature
  //  ['v', ZipStream::ZIP_VERSION_MADE_BY],  // Made by version
  //  ['v', $this->version->getValue()],      // Extract by version
  //  ['v', $this->bits],                     // General purpose bit flags -
  //  ['v', $this->method->getValue()],       // Compression method
  //  ['V', $time],                           // Timestamp (DOS Format)
  //  ['V', $this->crc],                      // CRC32
  //  ['V', $this->zlen->getLowFF()],         // Compressed Data Length
  //  ['V', $this->len->getLowFF()],          // Original Data Length
  //  ['v', strlen($name)],                   // Length of filename
  //  ['v', strlen($footer)],                 // Extra data len (see above)
  //  ['v', strlen($comment)],                // Length of comment
  //  ['v', 0],                               // Disk number
  //  ['v', 0],                               // Internal File Attributes
  //  ['V', 32],                              // External File Attributes
  //  ['V', $this->ofs->getLowFF()]           // Relative offset of local header

  std::vector<uint8_t> extra_data;
  if (size_ >= std::numeric_limits<uint32_t>::max()) {
    LE64(extra_data, size_);
  }
  if (compressed_size_ >= std::numeric_limits<uint32_t>::max()) {
    LE64(extra_data, compressed_size_);
  }
  if (in_archive_offset >= std::numeric_limits<uint32_t>::max()) {
    LE64(extra_data, in_archive_offset);
  }
  if (extra_data.size() > 0) {
    std::vector<uint8_t> ext;
    LE16(ext, 0x0001);
    LE16(ext, extra_data.size());
    extra_data.insert(extra_data.begin(), ext.begin(), ext.end());
  }

  std::vector<uint8_t> cdr{CDR_FILE_SIGNATURE};
  cdr.insert(cdr.end(), ZIP_VERSION_MADE_BY.begin(), ZIP_VERSION_MADE_BY.end());
  LE16(cdr, options_.version);
  LE16(cdr, config_bits_);
  LE16(cdr, options_.method);
  LE32(cdr, dos_time_);
  LE32(cdr, crc_);
  LE32(cdr, std::min<uint64_t>(compressed_size_, 0xFFFFFFFF));
  LE32(cdr, std::min<uint64_t>(size_, 0xFFFFFFFF));
  LE16(cdr, name_.size());
  LE16(cdr, extra_data.size());
  LE16(cdr, comment_.size());
  LE16(cdr, 0);
  LE16(cdr, 0);
  LE32(cdr, 32);
  LE32(cdr, std::min<uint64_t>(in_archive_offset, 0xFFFFFFFF));

  cdr.insert(cdr.end(), name_.begin(), name_.end());
  cdr.insert(cdr.end(), extra_data.begin(), extra_data.end());
  cdr.insert(cdr.end(), comment_.begin(), comment_.end());

  return cdr;
}

size_t File::getTotalSize() const { return total_size_; }

}  // namespace zip
