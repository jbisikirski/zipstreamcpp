#pragma once
#include <cstdint>
#include <vector>

#define LE16(vec, data)                                         \
  do {                                                          \
    uint16_t data_le = htole16(static_cast<uint16_t>(data));    \
    vec.push_back(static_cast<uint8_t>(data_le & 0xFF));        \
    vec.push_back(static_cast<uint8_t>((data_le >> 8) & 0xFF)); \
  } while (false)

#define LE32(vec, data)                                          \
  do {                                                           \
    uint32_t data_le = htole32(static_cast<uint32_t>(data));     \
    vec.push_back(static_cast<uint8_t>(data_le & 0xFF));         \
    vec.push_back(static_cast<uint8_t>((data_le >> 8) & 0xFF));  \
    vec.push_back(static_cast<uint8_t>((data_le >> 16) & 0xFF)); \
    vec.push_back(static_cast<uint8_t>((data_le >> 24) & 0xFF)); \
  } while (false)

#define LE64(vec, data)                                          \
  do {                                                           \
    uint64_t data_le = htole64(static_cast<uint64_t>(data));     \
    vec.push_back(static_cast<uint8_t>(data_le & 0xFF));         \
    vec.push_back(static_cast<uint8_t>((data_le >> 8) & 0xFF));  \
    vec.push_back(static_cast<uint8_t>((data_le >> 16) & 0xFF)); \
    vec.push_back(static_cast<uint8_t>((data_le >> 24) & 0xFF)); \
    vec.push_back(static_cast<uint8_t>((data_le >> 32) & 0xFF)); \
    vec.push_back(static_cast<uint8_t>((data_le >> 40) & 0xFF)); \
    vec.push_back(static_cast<uint8_t>((data_le >> 48) & 0xFF)); \
    vec.push_back(static_cast<uint8_t>((data_le >> 56) & 0xFF)); \
  } while (false)


namespace zip {
  /**
   * This number corresponds to the ZIP version/OS used (2 bytes)
   * From: https://www.iana.org/assignments/media-types/application/zip
   * The upper byte (leftmost one) indicates the host system (OS) for the
   * file.  Software can use this information to determine
   * the line record format for text files etc.  The current
   * mappings are:
   *
   * 0 - MS-DOS and OS/2 (F.A.T. file systems)
   * 1 - Amiga                     2 - VAX/VMS
   * 3 - *nix                      4 - VM/CMS
   * 5 - Atari ST                  6 - OS/2 H.P.F.S.
   * 7 - Macintosh                 8 - Z-System
   * 9 - CP/M                      10 thru 255 - unused
   *
   * The lower byte (rightmost one) indicates the version number of the
   * software used to encode the file.  The value/10
   * indicates the major version number, and the value
   * mod 10 is the minor version number.
   * Here we are using 6 for the OS, indicating OS/2 H.P.F.S.
   * to prevent file permissions issues upon extract (see #84)
   * 0x603 is 00000110 00000011 in binary, so 6 and 3
   */

  const std::vector<uint8_t> ZIP_VERSION_MADE_BY = {0x03, 0x06};
  /**
   * The following signatures end with 0x4b50, which in ASCII is PK,
   * the initials of the inventor Phil Katz.
   * See https://en.wikipedia.org/wiki/Zip_(file_format)#File_headers
   */
  const std::vector<uint8_t> FILE_HEADER_SIGNATURE = {0x50, 0x4b, 0x03, 0x04};
  const std::vector<uint8_t> CDR_FILE_SIGNATURE = {0x50, 0x4b, 0x01, 0x02};
  const std::vector<uint8_t> CDR_EOF_SIGNATURE = {0x50, 0x4b, 0x05, 0x06};
  const std::vector<uint8_t> DATA_DESCRIPTOR_SIGNATURE = {0x50, 0x4b, 0x07, 0x08};
  const std::vector<uint8_t> ZIP64_CDR_EOF_SIGNATURE = {0x50, 0x4b, 0x06, 0x06};
  const std::vector<uint8_t> ZIP64_CDR_LOCATOR_SIGNATURE = {0x50, 0x4b, 0x06,
                                                            0x07};
}
