#pragma once
#include <zip_stream/options.h>
#include <cstdint>
#include <cstdlib>
#include <string>
#include <vector>

namespace zip {
class ZipStream;
uint32_t dosTime(int64_t timestamp);
class File {
 private:
  ZipStream& zip_stream_;
  std::string name_;
  std::string comment_;
  size_t size_, compressed_size_, header_size_, footer_size_, offset_,
      total_size_;
  int32_t dos_time_;
  int32_t config_bits_;
  uint32_t crc_;
  file::Options options_;

  void addHeader();
  void addFooter();

 public:
  File(const std::string& name, ZipStream& zip_stream, const file::Options& options);
  ~File();
  void processData(const std::vector<uint8_t>& data);
  std::vector<uint8_t> getCDREntry(size_t in_archive_offset);
  size_t getTotalSize() const;
};
}  // namespace zip
