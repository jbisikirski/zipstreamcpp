#pragma once
/*
 * taken from
 * https://github.com/panzi/CRC-and-checksum-functions
 * with some changes
 */

#include <cstdint>
#include <vector>

uint32_t crc32buf(const std::vector<uint8_t>& data);
