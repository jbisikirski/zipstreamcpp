#pragma once
#include <chrono>
#include <string>

namespace zip {
enum class Version { kSTORE = 0x0A, kDEFLATE = 0x14, kZIP64 = 0x2D };
enum class Method { kSTORE = 0x00, kDEFLATE = 0x08 };
namespace file {
struct Options {
  std::string comment{};
  Method method{Method::kSTORE};
  Version version{Version::kSTORE};
  int64_t time{std::chrono::seconds(std::time(nullptr)).count()};
};
}  // namespace file
}  // namespace zip
