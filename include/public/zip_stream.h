#pragma once
/*
 * A cpp version of
 * https://github.com/maennchen/ZipStream-PHP
 */

#include <cstdint>
#include <cstdlib>
#include <mutex>
#include <vector>
#include <zip_stream/options.h>

namespace zip {
class ZipStream {
 private:
  std::string comment_;
  size_t file_count_, total_size_;
  std::vector<uint8_t> buffer_, cdr_;
  std::mutex mutex_;
  bool done_;

 public:
  explicit ZipStream(const std::string& comment = "");
  ~ZipStream();
  void addData(const std::vector<uint8_t>& data, const std::string& path,
               file::Options options = file::Options());
  void append(const std::vector<uint8_t>& data);
  void finalize();
  bool isDone() const;
  size_t getCurrentOffset() const;
  size_t fillData(uint8_t* const buffer, size_t max);
  size_t bufferSize() const;
};
}  // namespace zip
